package client;
//see bishop for more documentation as it acts similar
public class Pawn extends Piece{

	Pawn(char color, int row, int col){
		super(color,row,col);
		this.type='P';
	}
	//if actualMove is false, then return true or false, but don't move
	public boolean move(int destR,int destC, Piece[][] board, boolean actualMove){
		//BLACK pawn
		/*
            CASE 1: move 1 down:
            CASE 2: move 2 down:
            CASE 3: eat sideways
		 */

		System.out.println("DEBUG");
		int shortMove;
		int longMove;
		if (this.getColour()=='B'){
			shortMove=-1;
			longMove=-2;
		} else {
			shortMove=1;
			longMove=2;
		}
		//CASE 1: move 1 down
		if((destR+shortMove==this.row && destC==this.col) && (board[destR][destC]==null)){

			if (!actualMove) 
				return true;
			//assume nothing is there
			//update the board (generalize later)
			board[destR][destC] = board[row][col];
			board[row][col] = null;

			///////////new  
			//  this.row= destR;
			//  this.col = destC;


			if (this.getColour()=='B')
				this.row++;  //move me down
			else
				this.row--;
			//black ++ , white --
			return true;
		} 
		//CASE 2: move 2 down
		if((destR+longMove==this.row && destC==this.col) && (board[destR][destC]==null)){
			if (!actualMove) 
				return true;

			//make sure you are at row 1 (original location)


			if ((this.row!=1 && this.getColour()=='B') ||
					(this.row!=6 && this.getColour()=='W'))
			{  //if its black is 1, if its white is 6
				return false;
			}


			//check to ensure no one is in the way
			if (this.getColour()=='B'){
				if (board[destR-1][destC]!=null){ //change this
					return false;
				}
			} else {
				if (board[destR+1][destC]!=null){ //change this
					return false;
				}

			}

			if (!actualMove) 
				return true;


			//assume nothing is there
			//update the board (generalize later)
			board[destR][destC] = board[row][col];
			board[row][col] = null;




			//this.row+=2;  //move me down by 2 (new)
			if (this.getColour()=='B')
				this.row+=2;  //move me down
			else
				this.row-=2;

			return true;
		} 
		//CASE 3: eat
		//has to be the opposite color, but check later
		if ( (destR+shortMove==this.row  && Math.abs(destC-this.col)==1)  && (board[destR][destC]!=null)    ){

			if (!actualMove) 
				return true;


			//make the move, just like the other pieces.
			DrawEverything.remove(board[destR][destC]);
			board[destR][destC] = board[row][col];
			board[row][col] = null;
			//this.row++;

			if (this.getColour()=='B')
				this.row++;  //move me down
			else
				this.row--;
			this.col = destC;
			return true;
		}

		else {
			return false;
		}

	}

}
