package client;
import java.util.ArrayList;

import java.awt.*;


import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.JFrame;


//this class is responsible for drawing everything and loading images
//as well as message boxes for the UI.
public class DrawEverything {
    
    //the list of all the pieces currently on the board
    private static ArrayList<Piece> list  = new ArrayList<Piece>();
 
    //getter for the list above; returns only the valid pieces (the ones still on the board)
    public static ArrayList<Piece> getValidPieces(){
        return list;
    }
    
    private static Graphics gr=null;
    private static Container container;
    
    //set the graphics and the container in which to operate so we can draw.
    public static void setGraphics(Graphics g,Container c){
        gr = g;
        container=c;
    }
    
    //add a piece to the list
    public static void add(Piece object){
        list.add(object);
    }
    
    //remove the piece from the valid list, e.g. someone ate this piece
    public static void remove(Piece object){
            //if the piece we are removing is a King, means that someone ate the king
            //means game over. 
            if (object instanceof King){
                list.remove(object);
               DrawEverything.msgbox("Game over");
            } else {
                
                list.remove(object);
            }
     }
    
    //draws the board and the pieces
    public static void drawEverything(){
        if (gr==null){
            System.err.println("We never set the graphics quitting");
            System.exit(-1);
        }
        
        drawBoard();
     
        //draw every valid piece;  as simple as that.
        for (Piece p: list){
            drawPiece(p);
       
            //object.drawMeNew(gr);
        }
       
    }
    //draws a single piece
    private static void drawPiece(Piece p){
          try {
                
                File file = new File(p.getPath());  //have the path for the piece point to the proper file
                                                    //e.g. a white queen points to the white queen graphic
                BufferedImage image = ImageIO.read(file);//open the file
                int offsetX=115;
                int offsetY=100;
                //draw the piece at calculated xy, depending on the row and column
                gr.drawImage(image, p.getCol()*GUIBoard.tileSizeShare+offsetX, (p.getRow())*GUIBoard.tileSizeShare+offsetY +0, container);
                System.out.println("DREW PIECE");
            } catch (Exception e){
                System.err.println("CANNOT DRAW PIECE");
            }
    }
    
    /////////////////////// drawing helpers /////////////////////
    //draws only the board (no pieces).  to change the graphic, cannot change this, but change 
    //the actual image.  the chess graphics must be installed in the chess directory on drive c.
    private static void drawBoard(){
            try {
                File file = new File("./chess/board2.jpg"); //ONLY CHANGE THIS IF HAVE TO
                BufferedImage image = ImageIO.read(file);
                gr.drawImage(image, 50, 50, container);
                System.out.println("DREW BOARD");
            } catch (Exception e){
                System.err.println("CANNOT DRAW BOARD");
                System.exit(0);
            }
    }
    
    
    /////////////////////////////////////////////////////////////
    
    
   //show a message box with a custom message, unless we are supresing warnings.
    public static void msgbox(String s){
        if (!Board.suppressWarnings()){
            JOptionPane.showMessageDialog(null, s);
        }
    }   
    
}
