
package client;
import java.util.ArrayList;

/*
This class represents the board (non-graphical) data structure and the associated methods
 */


public class Board {
	//number of rows and columns on the board (the board is a square grid)
	private static final int ROWS = 8;
	private static final int COLS = 8;

	//declare a double dimension array to represent the board
	//if board[x][y] is null, means it is an empty spot, otherwise it has the piece stored here
	protected Piece [][] board;  //contain the actual pieces

	//set to true to ensure that messagesboxes do not popup when there is an invalid move
	private static boolean suppressWarnings = false;

	//getter for suppressWarnings
	public static boolean suppressWarnings(){
		return suppressWarnings;
	}


	private int moveNumber = 0;



	//this method creates all the pieces and puts them on the board
	private void createPieces(){
		//create black pawns, and put them in the second (1) row of the board
		for (int i=0;i<COLS;i++){
			Piece pawn = new Pawn('B',1,i);
			pawn.putOnBoard(board);  
		}

		//black queen
		Piece queenB = new Queen('B',0,3);
		queenB.putOnBoard(board);

		//black king
		Piece kingB = new King('B',0,4);
		kingB.putOnBoard(board);

		//black rooks
		Piece rookB1 = new Rook('B',0,0);
		rookB1.putOnBoard(board);

		Piece rookB2 = new Rook('B',0,7);
		rookB2.putOnBoard(board);

		//black knights
		Piece knightB1 = new Knight('B',0,1);
		knightB1.putOnBoard(board);

		Piece knightB2 = new Knight('B',0,6);
		knightB2.putOnBoard(board);

		//black bishops
		Piece bishopB1 = new Bishop('B',0,2);
		bishopB1.putOnBoard(board);

		Piece bishopB2 = new Bishop('B',0,5);
		bishopB2.putOnBoard(board);



		//create white pieces, same as black but at the bottom of the board 
		for (int i=0;i<COLS;i++){
			Piece pawn = new Pawn('W',6,i);
			pawn.putOnBoard(board);  
		}

		//white queen
		Piece queenW = new Queen('W',7,3);
		queenW.putOnBoard(board);

		//white king
		Piece kingW = new King('W',7,4);
		kingW.putOnBoard(board);

		//white rooks
		Piece rookW1 = new Rook('W',7,0);
		rookW1.putOnBoard(board);

		Piece rookW2 = new Rook('W',7,7);
		rookW2.putOnBoard(board);

		//white knights
		Piece knightW1 = new Knight('W',7,1);
		knightW1.putOnBoard(board);

		Piece knightW2 = new Knight('W',7,6);
		knightW2.putOnBoard(board);

		//white bishops
		Piece bishopW1 = new Bishop('W',7,2);
		bishopW1.putOnBoard(board);

		Piece bishopW2 = new Bishop('W',7,5);
		bishopW2.putOnBoard(board);


	}

	//empty constructor: initialize the board and creates the pieces and puts them on the board
	//remember, there is no graphic component here.
	Board(){
		this.board  = new Piece[ROWS][COLS];
		createPieces();
	}

	// "draws" the board using (in case there is no graphics, this will do).
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for (int r=0;r<ROWS;r++){
			for (int c=0;c<COLS;c++){
				sb.append("|");//line between grid cells
				if (board[r][c]==null){  //no piece found
					sb.append("   ");
				} else { //draw the piece
					sb.append(" " + board[r][c].toString() + " ");
				}

			}
			sb.append("|\n");
			sb.append("----------------------------------------\n");
		}
		return sb.toString();
	}
	//moves the piece found at startR,C to destR,C.  if network is true, this is coming
	//from the network, otherwise from the local computer via mouse click
	public boolean movePiece(/*char colour,*/ int startR, int startC, int destR, int destC,boolean network){
		//check if trying to move outside the board.
		if (destR < 0 || destR > 7 || destC < 0 || destC >7 ){
			DrawEverything.msgbox("Clicked outside the board.  Invalid move.");

			return false;
		}

		//is empty?
		if (board[startR][startC] == null){
			DrawEverything.msgbox("You clicked an empty spot.  Invalid move.");
			//   moveNumber++;
			return false;
		}
		//is user's correct color?
		char colour=' ';
		if ((moveNumber%2)==0){  //should be white, but black for now
			colour = 'B';
		} else {
			colour='W';
		}

		if (board[startR][startC].getColour() != colour){
			DrawEverything.msgbox("Not your piece. invalid move");

			return false;
		}


		//save the board to a temp variable
		//just put back
		//////////////////////////////////////////////Piece [][] tempBoard = copyBoard();
		boolean success = board[startR][startC].move(destR, destC, this.board,true);    //actual move
		if (!success){
			DrawEverything.msgbox("Invalid move");
			return false;
		} else {
			System.out.println(this);
			//second move , must REDRAW BOARD
			GUI_MainWindow.frameShared.repaint();

			moveNumber++;

			System.out.println("SUCCESSFUL MOVE");
			int fromIndex = startR * 8 + startC;
			int toIndex = destR * 8 + destC;
			String send = "" + fromIndex + "|" + toIndex;




			System.out.println("SENDING: " + send );

			//if this is a move intitiated by current user (not the networked)
			if (!network){
				Networking net = GUI_MainWindow.getNet();
				net.sendMove(fromIndex, toIndex);  //sends the move to the other player over network.
				
			}
			
			return true;
		}
	}
	//not used, but would be useful for virtual boards so we can look steps ahead




	public boolean isKingDeadNextMove(){
		//for every opponent's piece, try to move it on top of my king,
		//if the move is valid, return true, otherwise return false.

		char colour=' ';
		if ((moveNumber%2)==0){  //move #2 set to white, but 
			colour = 'W';
		} else {
			colour='B';
		}
		Piece myKing = null;

		//myKing found
		for (int r=0;r<8;r++){
			for (int c=0;c<8;c++){
				Piece myPiece = board[r][c];
				//not interested in our own pieces
				if (myPiece.getColour()==colour){
					if (myPiece.getType()=='K'){
						myKing = myPiece;
					}
				}

			}
		}

		for (int r=0;r<8;r++){
			for (int c=0;c<8;c++){
				Piece opponentPiece = board[r][c];
				//not interested in our own pieces
				if (opponentPiece.getColour()==colour){
					continue;
				}

				//try to kill the king, piece doesn't actually move
				boolean success = board[r][c].move(myKing.getRow(), myKing.getCol(), this.board,false);  
				return success;

			}
		}

		return false;
	}
}
