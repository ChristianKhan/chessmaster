// ROOK or castle

package client;
//see bishop for full documentation as this acts the same.
public class Rook extends Piece{
    Rook(char color, int row, int col){
        super(color,row,col);
        this.type='R';
    }
   
     public boolean move(int destR,int destC, Piece[][] board,boolean actualMove){
         
         //move left or right
         //move up or down
         //must be NOTHING between the current place and the distination
         //destination CAN be empty , or can have a piece, but that piece will be eatern
         
         
         //check if trying to stop on same color
         if (board[destR][destC]!=null && board[row][col]!=null){
         
            if (board[destR][destC].getColour() == board[row][col].getColour()){
                DrawEverything.msgbox("Cannot eat your own piece.");
                return false;
            }
         }
         
         //check if we are moving right
         if ((row == destR) && (destC > col)){
             boolean OK=true; //until we find otherwise
             for (int c=col+1;c<destC;c++){
                 if (board[row][c]!=null){
                     OK=false;
                     DrawEverything.msgbox("Cannot move this far right, something int the way.");
                     return false;
                 }
             }  //end of the loop
           
        } 
     
         
          //check if we are moving left
         else if ((row == destR) && (destC < col)){
             boolean OK=true; //until we find otherwise
             for (int c=col-1;c>destC;c--){
                 if (board[row][c]!=null){
                     OK=false;
                          DrawEverything.msgbox("Cannot move this far left, something int the way.");
                     return false;
                 }
             }  //end of the loop
         }
         //check if we are moving down
         else if ((destR>row) && (destC == col)){
             boolean OK=true; //until we find otherwise
             for (int r=row+1;r<destR;r++){
                 if (board[r][col]!=null){
                     OK=false;
                          DrawEverything.msgbox("Cannot move this far down, something int the way.");
                     return false;
                 }
             }  //end of the loop
           
         }
         
         
         //check if we are moving up
         else  if ((destR<row) && (destC == col)){
             boolean OK=true; //until we find otherwise
             for (int r=row-1;r>destR;r--){
                 if (board[r][col]!=null){
                     OK=false;
                           DrawEverything.msgbox("Cannot move rook to here, something in the way while moving down.");
                     return false;
                 }
             }  //end of the loop
           }
         else {
             DrawEverything.msgbox("Not a valid move");
               return false; 
         }
          
            
                if (!actualMove) 
                  return true;
           
          
           //make the move, just like the other pieces.     
           DrawEverything.remove(board[destR][destC]);
           board[destR][destC] = board[row][col];
            board[row][col] = null;
            this.col = destC;
            this.row = destR;
            return true;
     }
     
     
}
