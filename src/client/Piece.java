package client;

//the abstract class which is the parent of all pieces
public abstract class Piece {
    private char colour;    //W or B
    protected int row;      //the row and column where this piece is on the board
    protected int col;
    protected char type; //e.g. Q
    
    //getters
    public char getType(){
        return this.type;
    }
    public int getRow(){
        return row;
    }
    
    public int getCol(){
        return col;
    }
    
    //constructor
    Piece(char colour, int row, int col){
        this.colour = colour;
        this.row = row;
        this.col = col;
        DrawEverything.add(this);
    }
    
   // returns a conveniently structured path to the image of this piece.
      public String getPath(){
          // return "Q";
          System.out.println("./chess/" + this.getColour() + this.type + ".gif");
          return "./chess/" + this.getColour() + this.type + ".gif";  // WQ
          
       }  
      
    public char getColour(){
        return this.colour;
    }
    
    //put this piece on the board
    public void putOnBoard(Piece [][] board){
        board[this.row][this.col] = this;
    }
    public String toString(){
        
        //black piece -- capital, white piece lower.
        //the original is capital 
        if (this.colour=='W'){
            //convert and print
          return "" + Character.toLowerCase(type);
        } else {
         return "" + type;
        }
     
    }
      //this is the only method that must be overridden by every particular piece
       public abstract boolean move(int destR,int destC, Piece[][] board,boolean actualMove);

       public static Piece copyPiece(Piece original){
           Piece n=null;
           //uppercase
           switch (Character.toUpperCase(original.type)){
               case 'P':  n = new Pawn(original.colour, original.row,original.col); break;
               case 'Q':  n = new Queen(original.colour, original.row,original.col); break;
               case 'T':  n = new King(original.colour, original.row,original.col); break;
               case 'R':  n = new Rook(original.colour, original.row,original.col); break;
               case 'B':  n = new Bishop(original.colour, original.row,original.col); break;
               case 'K':  n = new Knight(original.colour, original.row,original.col); break;
           }
           
           return n;
       }

}
