package client;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.JFrame;

//this class contains main method responsible for:
//rendering board, "drawing" pieces, and connecting to network
//with a mouseListener for movement of pieces.


public class GUI_MainWindow extends JFrame implements MouseListener {

	//pointer to the neworking functionality
	private static Networking net = null;

	//getter for net
	public static Networking getNet(){
		return net;
	}

	public static JFrame frameShared;
	//if you change this, the coordinates will have to change throughout
	private static final int BOARD_WIDTH = 800;

	private static final int BOARD_HEIGHT = 800;
	//not sure what this is. likely added automatically by IDE ???
	private static final long serialVersionUID = 1L;

	//odd click means the first click of the move, the second click is the move is finished
	private int clickNumber=0;
	private RowCol originalFrom;



	private static GUIBoard guiBoard ;  



	//repainting    
	public void paint(Graphics g) {
		super.paint(g);
		frameShared=this;
		DrawEverything.setGraphics(g,this); //pass the graphics handle and this container to the drawEverything class
		DrawEverything.drawEverything();    //draw everything

	}

	public GUI_MainWindow() {
		setTitle("Chess Game");
		addMouseListener(this); //attach the mouse listener
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		if (guiBoard!=null){
			System.out.println("Mouse Clicked at X: " + x + " - Y: " + y);
			RowCol rc = guiBoard.convertToRow(x, y);
			System.out.println("ROW=" + rc.getRow() + " col=" + rc.getCol());

			if (this.clickNumber%2==0){
				//even move number, means that selecting from
				originalFrom = rc;
			} else {

				boolean success = guiBoard.movePiece(/*'B',*/originalFrom.getRow(),originalFrom.getCol(),rc.getRow(),rc.getCol(),false);

			}

			this.clickNumber++;

		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		System.out.println("Mouse Entered frame at X: " + x + " - Y: " + y);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		System.out.println("Mouse Exited frame at X: " + x + " - Y: " + y);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		System.out.println("Mouse Pressed at X: " + x + " - Y: " + y);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		System.out.println("Mouse Released at X: " + x + " - Y: " + y);
	}

	private static void createAndShowGUI() {

		//Create and set up the window.

		JFrame frame = new GUI_MainWindow();
		frame.setLayout(null); //////////////////// 
		//Display the window.
		frame.setSize(new Dimension(900,900));
		frame.setPreferredSize(new Dimension(900,900));
		frame.setVisible(true);
		//embedImage(frame);  /// try here
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		Container contentPane = frame.getContentPane();
		contentPane.setLayout(null);
		frame.repaint();
		guiBoard = new GUIBoard(800,800,50,50);



		NetworkMoveReader networkReader = new NetworkMoveReader(net.getInputStream(),guiBoard);
		networkReader.start();

	}

	public static void main(String[] args) throws Exception {

		net = new Networking();
		net.connect();


		//Schedule a job for the event-dispatching thread:

		//creating and showing this application's GUI.

		javax.swing.SwingUtilities.invokeLater(new Runnable() {

			public void run() { 	//MultiThreading here

				createAndShowGUI(); 

			}

		});
	}

}