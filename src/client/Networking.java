package client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

//this class deals with all things networking:
//handshaking between the two peers.
//the only thing this class doesn't do is wait for moves to come in
//this is done in NetworkMoveReader
public class Networking {
   
    //isServer must be set to true for the computer acting as the server, false otherwise
    //remember, that the program asks if the player is a server or not right off the bat.
    //only one server and only one client
    public static boolean isServer = false;
    
    private Scanner in = null;
    private PrintStream out = null;
    
    //getter for the input stream (in)
    public Scanner getInputStream(){
        if (in==null){
            System.err.println("Network not initialized.");
            System.exit(-1);
        }
        return in;
    }
    //send move, from and to index (this is a helper method)
    public void sendMove(int from, int to){
        if (out==null){
            System.err.println("You haven't initialized a connection.  Quitting.");
            System.exit(-1);
        }
        out.println("" + from );
        out.println("" + to);
       // out.println("" + from + "|" + to);
        out.flush();    //push it out
    }
    
    Networking(){;}
    
    //the user needs to know the IP address of the "server" so that the client can connect there
    private void getMyIP() throws Exception{
        System.out.println("My IP is: " + InetAddress.getLocalHost());
    }
    //just a random number likely unused.
    //can move this into the main method and ask the user instead.
    private static int PORT = 9997;
    
    //only for the server, open the socket
    private Socket openServerSocket(){
        ServerSocket listener = null; //socket to allow connections from clients
        Socket socket = null;
        try {
			listener = new ServerSocket(PORT);    //initialize to listed on the specified port
			socket = listener.accept();  //wait for a connection
                        
           }catch (Exception e){System.err.println("Error, quitting");System.exit(-1);}
        return socket;
    }
    
    //this is called by the client
    private Socket connectToServer(String serverIP) throws Exception {
        Socket soc = new Socket(serverIP, PORT);
        return soc;
    }
    
    //this is where the networking component starts
    public boolean connect() throws Exception{
        //ask if server or client
        Scanner sc = new Scanner(System.in);
        System.out.print("Are you a server or client? (S / C)");
        String choice = sc.nextLine();
        Socket commS = null;
        boolean isServe = false;
        if (choice.equals("S")){
            //if server, print out the IP address and then wait on a server socket for connection
                getMyIP();
                commS=openServerSocket();
                isServe = true;
                
        } else if (choice.equals("C")){
            System.out.print("Enter IP address of server: ");
            String serverIP = sc.nextLine();
            commS = connectToServer(serverIP);
               isServe = false;
            
        } else {
            System.out.println("Invalid choice.  Run again.");
            System.exit(-1);
        }
        
        //get out and in from commS;
        
        in = new Scanner(commS.getInputStream());

        out = new PrintStream(commS.getOutputStream(),true);

        Networking.isServer = isServe;
        return isServer;
    }
    
}
