package client;



//this class extends the board class and introduces gui to the game.
//although the actual drawing is centralized in DrawEverything.
public class GUIBoard extends Board {
    private int width, height;
    private int offsetX, offsetY;
    private int tileSize=0;
    public static int tileSizeShare=0;        

    GUIBoard(int width, int height, int offsetX,int offsetY){
        super(); //must be the first statement , polymorphism here    
        this.width = width;
        this.height = height;
        this.tileSize = 74;//width/8;// 74;//width/8; ///// 75?
        tileSizeShare=74;//this.tileSize;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }
    
   
    public RowCol convertToRow(int x,int y){
        offsetX=115; offsetY=100;
        int col = (x-offsetX)/tileSize;
        int row = (y-offsetY)/tileSize;
        RowCol rc = new RowCol(row,col);
        return rc;
    }
                       
}
