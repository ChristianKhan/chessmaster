package client;

/*
This class represents the chess bishop and therefore extends the Piece
*/

public class Bishop extends Piece{
    
    //initialize this bishop with a color and its location on the board
    Bishop(char color, int row, int col){
        super(color,row,col);   //pass all information into parent where it is stored.
        this.type='B';
    }
   
    //////////////////////////
    //Purpose: moves this piece on the board
    //Input:   destR/destC: the location on the board where this piece will move
    //         board: the board
    //         actualMove: if true, this is where the user is moving
    //                     if false, just querying to see if this move is valid, without 
    //                     actually moving
    //////////////////////////////////////////
    public boolean move(int destR,int destC, Piece[][] board,boolean actualMove){
        
//check if trying to stop on same color
         if (board[destR][destC]!=null && board[row][col]!=null){
         
            if (board[destR][destC].getColour() == board[row][col].getColour()){
                DrawEverything.msgbox("Cannot eat your own piece.");
                return false;
            }
         }
         
         //checks to ensure this is a PERFECT DIAGONAL.  height of move = width of move
        if (Math.abs(destR-row) != Math.abs(destC-col)){
                DrawEverything.msgbox("Not a valid diagonal move.");
                return false;
         }
         
        //moving up right
         if ((destR <  row) && (destC > col)){
             boolean OK=true; //until we find otherwise
             int r=row;
             for (int c=col+1;c<destC;c++){
                 r--;
                 if (board[r][c]!=null){
                     OK=false;
                     return false;
                 }
             }  //end of the loop
          
        }
 
         
         //check if we are moving down and right
         ///row -, col +
         else if ((destR >  row) && (destC > col)){
             boolean OK=true; //until we find otherwise
             int r=row;
             for (int c=col+1;c<destC;c++){
                 r++;
                 if (board[r][c]!=null){
                     OK=false;
                     return false;
                 }
             }  //end of the loop
               if (!actualMove) 
                  return true;
         }
 
         
          //check if we are moving down and left
         ///row -, col +
         else if ((destR >  row) && (destC < col)){
             boolean OK=true; //until we find otherwise
             int r=row;
             for (int c=col-1;c>destC;c--){
                 r++;
                 if (board[r][c]!=null){
                     OK=false;
                     return false;
                 }
             }  //end of the loop
               if (!actualMove) 
                  return true;
    }
 
         
         //check if we are moving up and left
         ///row -, col +
         else if ((destR <  row) && (destC < col)){
             boolean OK=true; //until we find otherwise
             int r=row;
             for (int c=col-1;c>destC;c--){
                 r--;
                 if (board[r][c]!=null){
                     OK=false;
                     return false;
                 }
             }  //end of the loop
               if (!actualMove) 
                  return true;
  
}
 
         else {
              DrawEverything.msgbox("Invalid move");
             return false;
         }
                 
         
         if (!actualMove)
             return true;
         
         //if here, actual move valid
         
         //remove the piece from its current location
            DrawEverything.remove(board[destR][destC]);
            //put the piece into its new location
            board[destR][destC] = board[row][col];
            board[row][col] = null;
            this.col = destC;
            this.row=destR;
         
         
         return true;
     }
        

}
