package client;
//see Bishop for full documentation, as this acts in a similar way

public class Queen extends Piece{
	Queen(char color, int row, int col){
		super(color,row,col);
		this.type='Q';
	}
	public boolean move(int destR,int destC, Piece[][] board,boolean actualMove){

		//check if trying to stop on same color
		if (board[destR][destC]!=null && board[row][col]!=null){

			if (board[destR][destC].getColour() == board[row][col].getColour()){
				DrawEverything.msgbox("Cannot eat your own piece.");
				return false;
			}
		}



		//checks to ensure this is a PERFECT DIAGONAL.  height of move = width of move
		if (!(
				(Math.abs(destR-row) == Math.abs(destC-col))   || 
				( destR == row ) ||
				(destC==col)
				))

		{
			DrawEverything.msgbox("Not a valid diagonal or vertical or horizontal move");
			return false;
		}

		//move left or right
		//move up or down
		//must be NOTHING between the current place and the distination
		//destination CAN be empty , or can have a piece, but that piece will be eatern


		//check if we are moving right
		if ((row == destR) && (destC > col)){
			boolean OK=true; //until we find otherwise
			for (int c=col+1;c<destC;c++){
				if (board[row][c]!=null){
					DrawEverything.msgbox("Cannot move queen to here, something in the way while moving right.");
					OK=false;
					return false;
				}
			}  //end of the loop
			if (!actualMove) 
				return true;
			DrawEverything.remove(board[destR][destC]);
			board[destR][destC] = board[row][col];
			board[row][col] = null;
			this.col = destC;
		}


		//check if we are moving left
		if ((row == destR) && (destC < col)){
			boolean OK=true; //until we find otherwise
			for (int c=col-1;c>destC;c--){
				if (board[row][c]!=null){
					OK=false;
					DrawEverything.msgbox("Cannot move queen to here, something in the way while moving left.");
					return false;
				}
			}  //end of the loop
			if (!actualMove) 
				return true;
			DrawEverything.remove(board[destR][destC]);
			board[destR][destC] = board[row][col];
			board[row][col] = null;
			this.col = destC;
		}

		//check if we are moving down
		if ((destR>row) && (destC == col)){
			boolean OK=true; //until we find otherwise
			for (int r=row+1;r<destR;r++){
				if (board[r][col]!=null){
					OK=false;
					DrawEverything.msgbox("Cannot move queen to here, something in the way while moving down.");
					return false;
				}
			}  //end of the loop
			if (!actualMove) 
				return true;
			DrawEverything.remove(board[destR][destC]);
			board[destR][destC] = board[row][col];
			board[row][col] = null;
			this.row = destR;
		}


		//check if we are moving up
		if ((destR<row) && (destC == col)){
			boolean OK=true; //until we find otherwise
			for (int r=row-1;r>destR;r--){
				if (board[r][col]!=null){
					OK=false;
					DrawEverything.msgbox("Cannot move queen to here, something in the way while moving down.");
					return false;
				}
			}  //end of the loop
			if (!actualMove) 
				return true;
			DrawEverything.remove(board[destR][destC]);
			board[destR][destC] = board[row][col];
			board[row][col] = null;
			this.row = destR;
		}





		///bishop style

		//check if we are moving up and right
		///row -, col +
		if ((destR <  row) && (destC > col)){
			boolean OK=true; //until we find otherwise
			int r=row;
			for (int c=col+1;c<destC;c++){
				r--;
				if (board[r][c]!=null){
					OK=false;
					DrawEverything.msgbox("Cannot move queen to here, something in the way while moving down.");

					return false;
				}
			}  //end of the loop
			if (!actualMove) 
				return true;
			DrawEverything.remove(board[destR][destC]);
			board[destR][destC] = board[row][col];
			board[row][col] = null;
			this.col = destC;
			this.row=destR;
		}


		//check if we are moving down and right
		///row -, col +
		if ((destR >  row) && (destC > col)){
			boolean OK=true; //until we find otherwise
			int r=row;
			for (int c=col+1;c<destC;c++){
				r++;
				if (board[r][c]!=null){
					OK=false;
					DrawEverything.msgbox("Cannot move queen to here, something in the way while moving down.");

					return false;
				}
			}  //end of the loop
			if (!actualMove) 
				return true;
			DrawEverything.remove(board[destR][destC]);
			board[destR][destC] = board[row][col];
			board[row][col] = null;
			this.col = destC;
			this.row=destR;
		}


		//check if we are moving down and left
		///row -, col +
		if ((destR >  row) && (destC < col)){
			boolean OK=true; //until we find otherwise
			int r=row;
			for (int c=col-1;c>destC;c--){
				r++;
				if (board[r][c]!=null){

					OK=false;
					DrawEverything.msgbox("Cannot move queen to here, something in the way while moving down.");

					return false;
				}
			}  //end of the loop
			if (!actualMove) 
				return true;
			DrawEverything.remove(board[destR][destC]);
			board[destR][destC] = board[row][col];
			board[row][col] = null;
			this.col = destC;
			this.row=destR;//r;
		}


		//check if we are moving up and left
		///row -, col +
		if ((destR <  row) && (destC < col)){
			boolean OK=true; //until we find otherwise
			int r=row;
			for (int c=col-1;c>destC;c--){
				r--;
				if (board[r][c]!=null){
					OK=false;
					DrawEverything.msgbox("Cannot move queen to here, something in the way while moving down.");

					return false;
				}
			}  //end of the loop
			if (!actualMove) 
				return true;

			//make the move, just like the other pieces.     
			DrawEverything.remove(board[destR][destC]);
			board[destR][destC] = board[row][col];
			board[row][col] = null;
			this.col = destC;
			this.row=destR;
		}

		return true;
	}

}
