package client;
//not used deprecated
import java.io.IOException;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;


//this class deals with the network: communicating the moves between clients
//and doing the initial handshake. the only thing it isn't doing, is sending the move
//done elsewhere

public class Network {
    
    private BufferedReader in;  //read from the network
    private PrintWriter out;    //write out to the network
    
    Network(int port){  //all computers must be using the same port (dynamically passed here)
        //initialize 
        try {
        Socket s = new Socket("localhost",port);    
        
        //initialize the in and out 
    	 in = new BufferedReader(new InputStreamReader(s.getInputStream(),"UTF-8")); 
			// out = new PrintWriter(s.getOutputStream(), true);
         out = new PrintWriter(new OutputStreamWriter(s.getOutputStream(), "UTF8"), true);
        
        } catch (Exception e){
            gotException(e);
        }
       }
    
    //handshake between the client and the server
    //*******deprecated, not needed*********
    public boolean login(String credentials){
        try {
            out.println(credentials); //send username|pasword
            out.flush();

            //read response

           String response = in.readLine(); 
           if (response.equals("OK")){
               return true;
           } else if (response.equals("BYE")){
               return false;
           } else {
               System.err.println("GOT INVALID RESPONSE");
               System.exit(-1);
           }
        } catch (Exception e){
            gotException(e);
        }
        return true;
    }
    
    private void gotException(Exception e){
        e.printStackTrace();
        System.exit(-1);
    }
    
    public ArrayList<String> getAvailablePlayers(){
        ArrayList<String> players = new ArrayList<String>();
       String str = null;
        try {
            str = in.readLine();
        } catch(Exception e){
            gotException(e);
        }
        
        String [] names = server.StringHelper.split(str, "|");
        for (String name: names){
            players.add(name);
        }
        return players;
    }
    
}
