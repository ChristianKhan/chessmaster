package client;
//this class is responsible for our knight piece, see bishop for comments as this acts similar
public class Knight extends Piece{
    
    Knight(char color, int row, int col){
        super(color,row,col);
        this.type='K';
    }
   
        public boolean move(int destR,int destC, Piece[][] board, boolean actualMove){
            
            
            //check if trying to stop on same color
         if (board[destR][destC]!=null && board[row][col]!=null){
         
            if (board[destR][destC].getColour() == board[row][col].getColour()){
                DrawEverything.msgbox("Cannot eat your own piece.");
                return false;
            }
         }
         
            
            
            // if absolute difference between rows is 2, then abs diff between column is 1
            //if abs diff between columns is 2, then diff between rows is 1
            
         if (( Math.abs(destR-row)==2 && Math.abs(destC-col)==1   )    ||
            ( Math.abs(destC-col)==2 && Math.abs(destR-row)==1   )) { 
             if (!actualMove) 
                  return true; 
            
             //move the piece, just like the other pieces
             DrawEverything.remove(board[destR][destC]);
             board[destR][destC] = board[row][col]; // <------------
             board[row][col] = null;
             this.col = destC;                   // <--------------
             this.row= destR;                    // <---------------------    
             
             return true;
         }  else {
                return false;
         }    
     }
        
    
}

