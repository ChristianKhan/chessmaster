package client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.util.Scanner;


//this class reads the moves that are sent to this player.
//corollary of the Network.java send method
public class NetworkMoveReader extends Thread{

    
    //the person who makes the first moves DECIDES FOR BOTH the color of each
     private static char myColor;
     
     public static void setMyColor(char color){
         if (color!='B' && color!='W'){
             System.err.println("Invalid use of setMyColor in NetworkMoveReader.  Quitting.");
             System.exit(-1);
         }
         myColor = color;
     }
     public static char getMyColor(){
         return myColor;
     }

    
    
    Scanner in ;
    private GUIBoard guiBoard;
    //constructor 
    NetworkMoveReader(Scanner in, GUIBoard guiBoard){
        this.in = in;
        this.guiBoard = guiBoard;
    }
    
    //Multithreading here; otherwise it would just hang the game
    //since the game can't wait on a move and allow the local user to interact
    public void run(){
        //ensure the in is intialized (or else can't listen for any data)
        if (in==null){
            System.err.println("Network not initialized.  Quitting.");
            System.exit(-1);
        }
        String received = null;
        String received2 = null;
        try {
            while(true){    //do this forever
        
            received = in.nextLine();   //first index - from
            received2 = in.nextLine();  //second index  - to
            System.out.println("==> " + received);
            int startR,startC, endR, endC;
            int start = Integer.parseInt(received);
            int end = Integer.parseInt(received2);
            startR = start / 8; //convert index back to row
            startC = start % 8;
            
            endR = end / 8;
            endC = end %8;
            

            
            System.out.println(startR + "|" + startC + " ==> " + endR +  "|" + endC);
          
            

            char aColor = guiBoard.board[startR][startC].getColour();
            if (aColor=='B'){
                NetworkMoveReader.setMyColor('W');
            } else if (aColor=='W'){
                NetworkMoveReader.setMyColor('B');
            } else {
                System.err.println("Invalid color.quitting.");
                System.exit(-1);
            }
            
            boolean success = guiBoard.movePiece(startR,startC,endR, endC,true);
            
            

            
            
            }
        }catch (Exception e){
            //might need it.
        }
        
    }
    
}
