package client;

//this is class is responsible for our king piece. for detailed comments see Bishop.java as
//it acts in a similar way

public class King extends Piece{
    //constructor: passes info to the parent.
    King(char color, int row, int col){
        super(color,row,col);
        this.type='T';
    }
    //see bishop for explanation
    public boolean move(int destR,int destC, Piece[][] board,boolean actualMove){
    
        
         if (board[destR][destC]!=null && board[row][col]!=null){
           //check if trying to stop on same color
         
            if (board[destR][destC].getColour() == board[row][col].getColour()){
                DrawEverything.msgbox("Cannot eat your own piece.");
                return false;
            }
         }
         
         //can move any direction,distance of one.
         
         //check horizontal
         if  (  (row == destR &&  Math.abs(col - destC) == 1   ) ||  //row, same, horizontal move by 1
               (col == destC &&  Math.abs(row - destR) == 1   ) ||  //col same, vertical move by 1
                 
                 ( //if my row and my column are different --> diagonal, then move one up/down + one left/right
                      (   Math.abs(col - destC) + 
                          Math.abs(row - destR)
                       ) == 2 && row!=destR && col!=destC 
                 )
                 
                 
                 )
         {
         if (!actualMove) 
                  return true; 
        
         //make the move, just like the other pieces
        DrawEverything.remove(board[destR][destC]); 
        board[destR][destC] = board[row][col];
           
         board[row][col] = null;
         this.col = destC;
         this.row = destR; 
         return true;
         } else {
             return false;
         }
         

     }
       
  
}
