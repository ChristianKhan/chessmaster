package server;
//This class converts string either from UTF-8 -> internal java string format
//or from interval java string format -> UTF-8 for consistency of code and no conflicts of formats
public class StringHelper {
 
    
    public static String[] split(String str, String separator){
        int index = str.indexOf(separator);
        if (index==-1){
            return null;
        } else {
            // hello there
            String [] ret = new String[2];
            ret[0]=str.substring(0,index);
            ret[1]=str.substring(index+1);
            return ret;
        }
    }
    
    
    // convert from UTF-8 -> internal Java String format
    public static String convertFromUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("ISO-8859-1"), "UTF-8");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }
 
    // convert from internal Java String format -> UTF-8
    public static String convertToUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }
}