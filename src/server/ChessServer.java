package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;


//the main server class
//allows multiple players (clients) to connect and play chess.
public class ChessServer {

	//the list of all authenticated players
	//this list contains only players that have successfully logged
	private static ArrayList <ServerPlayer> people; 

	//main method for the server
	public static void main(String[] args) {
		System.out.println("Chess Server Starting");

		int listenPort=0; //the port we will list on (and its passed in args as a parameter)

		//no port specified, so quit.
		if(args.length != 1) {
			System.err.println("Must Specify port #, Quitting....");
			System.exit(-1); 
		}

		//initalize the list of logged in people (0 to start)
		people = new ArrayList <ServerPlayer>(); 


		//Convert args string into integer; (port Number)
		listenPort=Integer.parseInt(args[0]);   //assume it is a number


		ServerSocket listener = null; //socket to allow connections from clients

		try {
			listener = new ServerSocket(listenPort);    //initialize to listed on the specified port
			while (true) {
				Socket socket = listener.accept();  //wait for a connection
				//initialize a serverplayer

				//do not add to the people array, because they are not logged in yet.
				ServerPlayer sp = new ServerPlayer(socket, people);
				sp.start(); //start the server player as a new thread 

			}
		} catch (IOException e) {
			//do more exception handling after
			e.printStackTrace();
		} 
	} 
}
