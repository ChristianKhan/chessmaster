package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

//this thread represents ONE player on the server
public class ServerPlayer extends Thread {
	
	private ArrayList<ServerPlayer> players; //list of all players (not a copy, but a pointer to the 
                                                 //authenticated players list
        
	private String playerName;  //name of this particular player
	
	//in and out communication channels for this player
        private PrintWriter out;   
	private BufferedReader in; 
	
        //getter for the playerName
        public String getPlayerName(){
            return playerName;
        }
        
        //constructor 
        //s is the socket that we are communicating with this player
        //players is the list of all players
	 ServerPlayer(Socket s, ArrayList<ServerPlayer> players ){
	     super();       //call constructor of thread
             this.players=players; 
		 try
		 {
                      //initialize the in & out
			 in = new BufferedReader(new InputStreamReader(s.getInputStream(),"UTF-8")); 
			// out = new PrintWriter(s.getOutputStream(), true);
                        out = new PrintWriter(new OutputStreamWriter(s.getOutputStream(), "UTF8-8"), true);
		 } catch (IOException e) {
                    e.printStackTrace(); 
		    System.exit(-1);
                 } 
         }
	
	 
	//given the credentials (c[0] is username, c[1] is password)
         //return the name of the payer in the database or return null if not found
         
	private String queryName(String [] credentials){
            //need to look up in the database, but for now just return name
            if (credentials[0].equals("bob@yahoo.com") && credentials[1].equals("password1")){
                return "Bob";
            } else if (credentials[0].equals("cathy@yahoo.com") && credentials[1].equals("password1")){
                return "Cathy";
            } else {
                return null;
            }
        }
        
        
	public void run() { 
                try {
                    // PROTOCOL: 3 --> 4
                    String upass = in.readLine(); //read Bob|password1 
                   // String upass2 = StringHelper.convertFromUTF8(upass);
                    String [] credentials = StringHelper.split(upass, "|");
                    //check in the database and return name if exists
                    playerName = queryName(credentials);
                    if (playerName==null) {  //invalid credentials, disconnect
                        disconnectPerson();
                        return;
                    } else {
                            //we have a proper person.
                            this.players.add(this);
                            //tell the user that they are OK
                            out.println("OK"); //protocol 4
                            out.flush();  //actually push it through (no buffering)

                            // send list of available players
                            PlayerSender ps = new PlayerSender(out,players);
                            ps.start();
                            
                            ///////////stop here

                            //wait and read the name of the person they want to play with

                            while (true) { //allow the user to pick another name
                                String pickedPlayerName = in.readLine();

                                //find the requested player and tell their serverplayer to play
                                ServerPlayer otherPlayer = findPlayer(pickedPlayerName);
                                if (otherPlayer==null){
                                    out.println("PLAYER LEFT"); out.flush();

                                } else {
                                    //we need to tell the other person.
                                    boolean isGame = otherPlayer.willPlay(this);
                                    if (isGame){
                                        ServerGame serverGame = new ServerGame(this,otherPlayer);
                                        serverGame.run(); //or .start
                                    }
                                }
                        }
                    }
                } catch (IOException e){
                    disconnectPerson();
                    return;
                }
	
	}
        //indicates an agreement to play
        private boolean willPlay(ServerPlayer other){
            return true;
        }
        
        //find a ServerPlayer with a particular username, or return null
        private ServerPlayer findPlayer(String name){
            ServerPlayer ret = null;
            for (ServerPlayer sp: players){
               if (sp.getPlayerName().equals(name)){
                   return sp;
               } 
            }
            return null; //nobody by that name found
        }
        
        
        //exception with this user, to kick them off
        public void disconnectPerson(){
            out.println("BYE"); //protocol 4
            out.flush();
        }
        
        //sends a particular message to this player
        public void sendMessage(String msg){
            out.println(msg);
            out.flush();
        }
        
        //wait for a message containing a move from this player.
        public String getMove(){
            try{
                String move = in.readLine();
                return move;
            } catch (Exception e){
                e.printStackTrace();
                return "";
            }
        }
}

//get username & password
// validate in database
// retrieve name from database
// if wrong kick out
// send list of available players
// wait for client to pick name
// Start the game