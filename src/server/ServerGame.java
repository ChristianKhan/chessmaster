package server;

//this class facilitates one game between two players
public class ServerGame extends Thread{
    
    private ServerPlayer player1;   //the first player (goes first)
    private ServerPlayer player2;   //the second player
    
    ServerGame(ServerPlayer player1, ServerPlayer player2){
        super();    //call constructor of Thread
        this.player1 = player1;
        this.player2 = player2;
    }
    
    public void run(){
        //inviter always goes first.
        player1.sendMessage("GO");
        
        //wait and read the move from player 1
        while(isWin()) { //until game over
            String move = player1.getMove();
            player2.sendMessage(move);

            String move2= player2.getMove();
            player1.sendMessage(move2);
        }
        
        
        
    }
    //modifiy this method later to actually see if someone won.
    private boolean isWin(){
        return false;
    }
}
