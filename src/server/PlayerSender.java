package server;

import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

//////////////////////////
//  This class periodically sends the list of all other players to a particular player
//  e.g. if Bob, Sally, and Cindy are online and we are sending to bob, then we are sending
//  Sally|Cindy
//////////////////////////////////////
public class PlayerSender extends Thread {
    
    private ArrayList<ServerPlayer> players;  //the list of all the players
    private PrintWriter out;                  //write out to the player through this
    
    PlayerSender(PrintWriter out, ArrayList<ServerPlayer> players){
        super();    //call the Thread constructor
        this.players = players;
        this.out = out;
    }
    
    //we may run this as a thread or as a regular method, not sure yet.
    public void run(){
        while(true){
            sendAllPlayers();   //send the players
            try{
                this.sleep(5000);   // wait 5 seconds
            } catch (InterruptedException e){
                return;             //someone interrupted us, do something.
            }
        }
    }
    
    
    private void sendAllPlayers(){
            //use StringBuilder to efficientily build the list of the players
            StringBuilder sb = new StringBuilder();
            
            for (ServerPlayer sp: players){
                if (sp != null){
                    sb.append(sp.getPlayerName() + "|");  //separate the names with a |
                } 
            }
            // "Bob|Cathy|Cindy|"
            //if there are any players to send, delete the last |
            if (sb.length()>0){
                sb.deleteCharAt(sb.length()-1);
                out.println(sb.toString());     //send
            }  else {
                out.println("NOBODY HERE");     //send
            }
            out.flush();    //flush out the buffer just in case
        }
}
